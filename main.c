#include <gtk/gtk.h>

static void
mtu_changed (GtkSpinButton *mtu,
             GtkWidget *bytes_label)
{
    if (gtk_spin_button_get_value_as_int (mtu) == 0)
    {
        gtk_widget_hide (bytes_label);
    }
    else
    {
        gtk_widget_show (bytes_label);
    }
}

int
main (int argc, char **argv)
{
    GtkBuilder *builder;
    GtkWidget *bytes_label;
    GtkWidget *spin_mtu;
    GtkWidget *window;

    gtk_init (&argc, &argv);

    builder = gtk_builder_new_from_file ("main.ui");

    window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
    bytes_label = GTK_WIDGET (gtk_builder_get_object (builder, "label_mtu"));
    spin_mtu = GTK_WIDGET (gtk_builder_get_object (builder, "spin_mtu"));

    g_signal_connect (spin_mtu, "value-changed",
                      G_CALLBACK (mtu_changed), bytes_label);
    g_signal_connect (window, "unmap",
                      G_CALLBACK (gtk_main_quit), NULL);

    gtk_widget_show (window);

    gtk_main ();

    g_object_unref (builder);

    return 0;
}
